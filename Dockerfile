FROM node:16-alpine as base
WORKDIR /usr/src/app


FROM base AS dependencies
COPY package*.json ./
RUN npm install

FROM base AS builder
WORKDIR /usr/src/app
COPY . .
COPY --from=dependencies /usr/src/app/node_modules ./node_modules
RUN npm run build



FROM nginx:alpine
RUN rm -rf /usr/share/nginx/html/*

COPY --from=builder /usr/src/app/dist/lib /usr/share/nginx/html/lib
COPY --from=builder /usr/src/app/dist/index.html /usr/share/nginx/html/index.html
COPY ./.nginx/nginx.conf /etc/nginx/nginx.conf

ENTRYPOINT ["nginx", "-g", "daemon off;"]
